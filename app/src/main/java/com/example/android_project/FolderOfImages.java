package com.example.android_project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class FolderOfImages extends AppCompatActivity implements AdapterView.OnItemClickListener{
    private int classify;
    private long userId;
    private DBHelper db;
    private ImageAdapter adapter;
    private ListView listView;
    private ArrayList<ModelImage> images;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder_of_images);
        this.db = new DBHelper(this);
        this.listView = findViewById(R.id.list);
        this.listView.setOnItemClickListener(this);
        Bundle extras=getIntent().getExtras();
        String name=extras.getString("idval"); //if data you are sending is String.
        this.userId =extras.getLong("idval");

        Bundle extras1=getIntent().getExtras();
        String name1=extras.getString("classify");
        this.classify =extras.getInt("classify");

        //get all images
        this.images = this.db.findImages(this.userId, this.classify);
        this.adapter = new ImageAdapter(this, this.images);
        this.listView.setAdapter(this.adapter);
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
    {
        ModelImage edit_image = this.images.get(position);
        Intent intent = new Intent(this, EditImagePage.class);
        intent.putExtra("name", edit_image.getName());
        intent.putExtra("id", edit_image.get_id());
        intent.putExtra("classify", edit_image.getClassify());
        intent.putExtra("blob", edit_image.getBitmap());
        intent.putExtra("idval", this.userId);


        startActivity(intent);
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
      getMenuInflater().inflate(R.menu.my_menu, menu);
      return true;
    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        int id = item.getItemId();
        System.out.println(id);
        if(id == R.id.menuStart)
        {
            Toast.makeText(this, " Good bye!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        }
        else if(id == R.id.backPage)
        {
            finish();
        }
        return true;
    }

}