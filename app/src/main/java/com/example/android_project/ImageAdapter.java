package com.example.android_project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class ImageAdapter extends ArrayAdapter<ModelImage> {
    private Context context;
    private ArrayList<ModelImage> list;
    public ImageAdapter(@NonNull Context context, ArrayList<ModelImage> list)
    {
        super(context, R.layout.row_image, list);

        this.context = context;
        this.list = list;
    }
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
        if(view == null){
            view = inflater.inflate(R.layout.row_image, viewGroup,false);
        }

        TextView name = view.findViewById(R.id.name);
        ImageView image = view.findViewById((R.id.imageView3));

        ModelImage imageObject = list.get(i);

        name.setText(imageObject.getName());
        image.setImageBitmap(imageObject.getBitmap());


        return view;
    }
}
