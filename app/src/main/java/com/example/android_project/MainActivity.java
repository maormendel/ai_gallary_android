package com.example.android_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btnRegister;
    private Button btnLogin;

    private DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.db = new DBHelper(this);
        this.btnLogin = (Button)findViewById(R.id.login_id);
        this.btnRegister = (Button)findViewById(R.id.register_id);



    }



    public void btnRegister(View v)
    {
        Intent intent = new Intent(this, registerPage.class);
        startActivity(intent);
        //finish();
    }
    public void btnLogin(View v)
    {
        Intent intent = new Intent(this, loginPage.class);
        startActivity(intent);
        //finish();
    }
}

