package com.example.android_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class loginPage extends AppCompatActivity {
    private EditText username;
    private EditText password;
    private DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        this.username = (EditText)findViewById(R.id.username);
        this.password = (EditText)findViewById(R.id.password);
        this.db = new DBHelper(this);
    }

    public void btnExitIntent(View v)
    {
        finish();
        return;
    }


    public void btnSendLogin(View v)
    {
        //Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        //startActivity(intent);
        String username = this.username.getText().toString();
        String password = this.password.getText().toString();

        //check that both names are filled
        if(username.trim().equals(""))
        {
            Toast.makeText(this, " You have to enter a name", Toast.LENGTH_SHORT).show();
        }
        else if(password.trim().length() < 8)
        {
            Toast.makeText(this, " You have to enter a password that is at least 8 characters", Toast.LENGTH_SHORT).show();
        }
        else
        {
            ModelUser m = this.db.findUserWithPassword(username, password);
            if(m.getUsername() != "")
            {
                Toast.makeText(this, "successfuly login", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, chooseOption.class);
                System.out.println("id5: " + m.get_id());
                intent.putExtra("idval", m.get_id());
                intent.putExtra("name", m.getUsername());
                startActivity(intent);
                finish();
                //crate new intent and pass the user to the next page so it can used the other parameters
            }
            else
            {
                Toast.makeText(this, "could not find a user or the password is incorrect", Toast.LENGTH_SHORT).show();
            }
        }
    }
}