package com.example.android_project;

import android.graphics.Bitmap;

import java.sql.Date;

public class ModelImage {
    private long _id;
    private String name;
    private Bitmap bitmap;
    //private String description;
    private int classify;
    //private String image_path;
    //private ModelImage m;
    //private Date image_date;

    public ModelImage(long _id, String name, Bitmap bitmap, int classify) {
        this._id = _id;
        this.name = name;
        this.bitmap = bitmap;
        this.classify = classify;
    }

    public long get_id(){
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int getClassify() {
        return classify;
    }

    public void setClassify(int classify) {
        this.classify = classify;
    }
}
