package com.example.android_project;

public class ModelUser {

    private long _id;
    private String username;
    private String password;
    private String email;


    public ModelUser(long _id, String username, String password, String email) {
        this._id = _id;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
