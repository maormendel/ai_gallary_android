package com.example.android_project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ImageCategrories extends AppCompatActivity implements AdapterView.OnItemClickListener  {
private String username;
private int id;
private long user_id;
private ImageView iv;
private ArrayList<Integer> imageList;
private int choose = -1;
ArrayList<String> categroies;
ArrayAdapter<String> adapter;
ListView listView;
TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_categrories);


            Bundle extras=getIntent().getExtras();
            String name=extras.getString("idval");
            this.user_id =extras.getLong("idval");


            this.categroies = new ArrayList<>();
            this.categroies.add("buildings");
            this.categroies.add("forest");
            this.categroies.add("glacier");
            this.categroies.add("mountain");
            this.categroies.add("sea");
            this.iv = (ImageView)findViewById(R.id.iv);

            this.imageList = new ArrayList<>();

            imageList.add(R.drawable.buildings);
            imageList.add(R.drawable.forest);
            imageList.add(R.drawable.glacier);
            imageList.add(R.drawable.mountain);
            imageList.add(R.drawable.sea);


        adapter=new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,categroies);

        listView = (ListView)findViewById(R.id.lvSimple);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        this.iv.setImageResource(imageList.get(position));
        this.choose = position;
        System.out.println(position);
    }

    public void btnEnterCategory(View v)
    {
        if(this.choose == -1)
            Toast.makeText(this, "Chose a folder to enter", Toast.LENGTH_SHORT).show();
        else
        {
            Intent intent = new Intent(this, FolderOfImages.class);
            intent.putExtra("idval", this.user_id);
            intent.putExtra("classify", this.choose);
            startActivity(intent);

        }

    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        int id = item.getItemId();
        System.out.println(id);
        if(id == R.id.menuStart)
        {
            Toast.makeText(this, " Good bye!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        }
        else if(id == R.id.backPage)
        {
            finish();
        }
        return true;
    }
}