package com.example.android_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class registerPage extends AppCompatActivity {
    private EditText username;
    private EditText password;
    private EditText email;
    private DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);
        this.email = (EditText)findViewById(R.id.email);
        this.username = (EditText)findViewById(R.id.username);
        this.password = (EditText)findViewById(R.id.password);
        this.db = new DBHelper(this);

    }
    public void btnExitIntent(View v)
    {
        finish();
        return;
    }
    public void btnSendRegister(View v)
    {
        String username = this.username.getText().toString();
        String password = this.password.getText().toString();
        String email = this.email.getText().toString();



        //check that both names are filled
        if(username.trim().equals(""))
        {
            Toast.makeText(this, " You have to enter a name", Toast.LENGTH_SHORT).show();
        }
        else if(password.trim().length() < 8)
        {
            Toast.makeText(this, " You have to enter a password that is at least 8 characters", Toast.LENGTH_SHORT).show();
        }
        else if(email.trim().equals(""))
        {
            Toast.makeText(this, " You have to enter an email", Toast.LENGTH_SHORT).show();
        }
        else
        {
            if(this.db.findUser(username).getUsername() != "")
            {
                Toast.makeText(this, "there is already a user with that name", Toast.LENGTH_SHORT).show();
            }
            else
            {
                ModelUser m = this.db.insert(new ModelUser(0,username, password, email));
                if(m.getUsername() != username)
                    Toast.makeText(this, "someething when wrong", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "user succesfully created", Toast.LENGTH_SHORT).show();
            }
        }
    }
}