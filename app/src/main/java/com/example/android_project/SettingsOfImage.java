package com.example.android_project;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class SettingsOfImage extends AppCompatActivity {
    private ImageView iv;
    private EditText name;
    private ActivityResultLauncher<Intent> activityResultLauncher;
    private Bitmap bitmapSave;
    private DBHelper db;
    private Spinner spinner;
    private long id;
    private int classify = 0;
    private Map<String, Integer> options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_of_image);

        this.db = new DBHelper(this);
        Bundle extras=getIntent().getExtras();
        String name=extras.getString("idval"); //if data you are sending is String.
        this.id =extras.getLong("idval"); //if data you are sending is integer.        System.out.println("id: " + this.id);
        System.out.println("fffff: " + this.id);


        this.spinner = findViewById(R.id.spinner);
        String[] items = new String[]{"buildings", "forest", "glacier", "mountain", "sea"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        this.spinner.setAdapter(adapter);

        this.options = new HashMap<String, Integer>();
        this.options.put("buildings", 0);
        this.options.put("forest", 1);
        this.options.put("glacier", 2);
        this.options.put("mountain", 3);
        this.options.put("sea", 4);

        this.activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == RESULT_OK && result.getData() != null)
                {
                    Bundle bundle = result.getData().getExtras();
                    Bitmap bitmap = (Bitmap) bundle.get("data");
                    iv.setImageBitmap(bitmap);
                    bitmapSave = bitmap;



                }
            }
        });
        this.name = findViewById(R.id.nameImage);
        this.iv = findViewById(R.id.imageView2);
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        if(true) //should be intent.resolveActivity(getPackageManager())!=null
        {
            //activate the camera to get a picture
            //startActivityForResult(intent,1);\activityResultLauncher
            activityResultLauncher.launch(intent);

        }
        this.iv.setImageBitmap(bitmapSave);
    }

    public void saveImage(View v)
    {
        String text = this.spinner.getSelectedItem().toString();
        int classifyOption = this.options.get(text);
        System.out.println(classifyOption);
        ModelImage m = new ModelImage(0, this.name.getText().toString(), bitmapSave, classifyOption);
        System.out.println("ggggg: " + this.id);
        ModelImage newImage = this.db.insertImage(m, this.id);
        System.out.println(newImage.getName());
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        int id = item.getItemId();
        System.out.println(id);
        if(id == R.id.menuStart)
        {
            Toast.makeText(this, " Good bye!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        }
        else if(id == R.id.backPage)
        {
            finish();
        }
        return true;
    }

}