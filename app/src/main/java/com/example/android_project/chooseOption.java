package com.example.android_project;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class chooseOption extends AppCompatActivity {
    private long id;
    private String username;
    private ImageView iv;
    private ActivityResultLauncher<Intent> activityResultLauncher;
    private Bitmap bitmapSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_option);
        Bundle extras=getIntent().getExtras();
        String name=extras.getString("idval"); //if data you are sending is String.
        this.id =extras.getLong("idval"); //if data you are sending is integer.
        System.out.println("id: " + this.id);

        this.username = this.getIntent().getStringExtra("name");
        this.iv = findViewById(R.id.imageView);


        this.activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == RESULT_OK && result.getData() != null)
                {
                    Bundle bundle = result.getData().getExtras();
                    Bitmap bitmap = (Bitmap) bundle.get("data");
                    iv.setImageBitmap(bitmap);
                    bitmapSave = bitmap;



                }
            }
        });
    }




    public void btnOpenCamera(View v)
    {
        Intent intent = new Intent(this, SettingsOfImage.class);
        intent.putExtra("idval", this.id);
        startActivity(intent);
    }

    public void btnOpenCategory(View v)
    {
        Intent intent = new Intent(this, ImageCategrories.class);
        intent.putExtra("idval", this.id);
        startActivity(intent);
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        int id = item.getItemId();
        System.out.println(id);
        if(id == R.id.menuStart)
        {
            Toast.makeText(this, " Good bye!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        }
        else if(id == R.id.backPage)
        {
            finish();
        }
        return true;
    }
/*
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                this.iv.setImageBitmap(imageBitmap);
            }
        }
    }
   */

}
