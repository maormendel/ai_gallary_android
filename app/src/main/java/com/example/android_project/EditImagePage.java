package com.example.android_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class EditImagePage extends AppCompatActivity {
    private static final int PICKFILE_REQUEST_CODE = 1;

    private ImageView iv;
    private EditText editName;
    private Spinner spinner;
    private Button updateBtn;
    private Button deleteBtn;
    private DBHelper db;
    private ImageView img;
    private String name;
    private long id;
    private int classify;
    private Map<String, Integer> options;
    private long userId;
    private Uri uri;
    private byte[] inputData;
    private Bitmap loaded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_image_page);
        this.db = new DBHelper(this);

        Bundle extras = getIntent().getExtras();
        this.userId =extras.getLong("idval");
        this.name = extras.getString("name");
        this.id = extras.getLong("id");
        this.classify = extras.getInt("classify");
        this.editName = findViewById(R.id.nameImageEdit);
        this.editName.setText(this.name);
        Bitmap bitmap = (Bitmap) getIntent().getParcelableExtra("blob");
        this.loaded = bitmap;
        this.img = findViewById(R.id.imageView4);
        this.img.setImageBitmap(bitmap);
        this.uri = null;


        this.options = new HashMap<String, Integer>();
        this.spinner = findViewById(R.id.spinnerEdit);
        String[] items;
        if(classify == 0)
            items = new String[]{"buildings", "forest", "glacier", "mountain", "sea"};
        else if(classify == 1)
            items = new String[]{"forest", "buildings", "glacier", "mountain", "sea"};
        else if(classify == 2)
            items = new String[]{"glacier", "buildings", "forest", "mountain", "sea"};
        else if(classify == 3)
            items = new String[]{"mountain", "buildings", "forest", "glacier",  "sea"};
        else
            items = new String[]{"sea", "buildings", "forest", "glacier", "mountain"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        this.spinner.setAdapter(adapter);


        this.options.put("buildings", 0);
        this.options.put("forest", 1);
        this.options.put("glacier", 2);
        this.options.put("mountain", 3);
        this.options.put("sea", 4);

    }
    public void deleteItem(View v)
    {
        if(this.db.removeImage(this.id))
        {
            Toast.makeText(this, " deleted!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "something did not work", Toast.LENGTH_SHORT).show();
        }
        Intent intent = new Intent(this, chooseOption.class);
        intent.putExtra("idval", this.userId);
        startActivity(intent);
    }
    public void updateItem(View v)
    {

        String text = this.spinner.getSelectedItem().toString();
        Bitmap newBitemap;
        int classifyOption = this.options.get(text);
        if(this.inputData != null)
        {
            newBitemap = BitmapFactory.decodeByteArray(this.inputData, 0, this.inputData.length);

        }
        else
        {
            newBitemap = this.loaded;
        }
        ModelImage m = new ModelImage(0, this.editName.getText().toString(), this.loaded, classifyOption);
        this.db.removeImage(this.id);
        ModelImage newImage = this.db.insertImage(m, this.userId);
        Intent intent = new Intent(this, chooseOption.class);
        intent.putExtra("idval", this.userId);
        startActivity(intent);
    }

    public void uploadFileClicked(View v)
    {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        // Ask specifically for something that can be opened:
        chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
        chooseFile.setType("*/*");
        startActivityForResult(
                Intent.createChooser(chooseFile, "Choose a file"),
                PICKFILE_REQUEST_CODE
        );
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_REQUEST_CODE && resultCode == RESULT_OK) {
            this.uri = data.getData();
        }
        else
        {
            Toast.makeText(this, "Did not choose file correctly", Toast.LENGTH_SHORT).show();
        }
        String path = this.uri.getPath();
        System.out.println(path);

            InputStream iStream = null;
            try {
                iStream = getContentResolver().openInputStream(uri);
                this.inputData = this.getBytes(iStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }



    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
            System.out.println("what what");
        }
        return byteBuffer.toByteArray();
    }

}