package com.example.android_project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASENAME = "users.db";
    private static final String TABLE_RECORD = "tblusers";
    private static final int DATABASEVERSION = 1;

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_EMAIL = "email";

    private static final String COLUMN_IMAGE_PATH = "image_path";
    private static final String CLASSIFIY = "classify";
    private static final String DATE_IMAGE = "date_image";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String USERID = "UserID";


    private static final String[] allColumns = {COLUMN_ID, COLUMN_PASSWORD, COLUMN_EMAIL, COLUMN_USERNAME};

    private static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " +
            TABLE_RECORD + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_USERNAME + " TEXT," +
            COLUMN_EMAIL + " TEXT," +
            COLUMN_PASSWORD + " TEXT );";

    private static final String CREATE_TABLE_IMAGES = "CREATE TABLE IF NOT EXISTS tblimages(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            "classify INTEGER," +
            "name TEXT," +
            "image BLOB, " +
            "UserID INTEGER, " +
            "FOREIGN KEY (UserID) REFERENCES tblusers(_id));";


    private SQLiteDatabase database; // access to table

    public DBHelper(@Nullable Context context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
    }


    // creating the database
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_USER);
        sqLiteDatabase.execSQL(CREATE_TABLE_IMAGES);

    }

    // in case of version upgrade -> new schema
    // database version
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RECORD);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS tblimages");

        onCreate(sqLiteDatabase);
    }

    // get the user back with the id
    // also possible to return only the id
    public ModelUser insert(ModelUser user) {
        database = getWritableDatabase(); // get access to write the database
        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, user.getUsername());
        values.put(COLUMN_PASSWORD, user.getPassword());
        values.put(COLUMN_EMAIL, user.getEmail());
        long id = database.insert(TABLE_RECORD, null, values);
        user.set_id(id);
        database.close();
        return user;
    }

    public ModelUser findUser(String userName) {
        database = getReadableDatabase(); // get access to read the database
        // Two options,
        // since query cannot be created in compile time there is no difference
        //Cursor cursor = database.rawQuery(query, values);
        String[] usernames = {userName};
        Cursor cursor = database.query(TABLE_RECORD, allColumns, COLUMN_USERNAME + " = ? ", usernames, null, null, null); // cursor points at a certain row
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_USERNAME));

                String userPass = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
                String email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                ModelUser user = new ModelUser(id, name, userPass, email);
                return user;
            }// end while
        } // end if
        database.close();


        ModelUser m = new ModelUser(0, "", "", "");
        return m;
    }

    public ModelUser findUserWithPassword(String username, String password) {
        database = getReadableDatabase(); // get access to read the database
        // Two options,
        // since query cannot be created in compile time there is no difference
        //Cursor cursor = database.rawQuery(query, values);
        String select = "SELECT * FROM " + TABLE_RECORD + " WHERE " + COLUMN_USERNAME + " = ? AND " + COLUMN_PASSWORD + " = ?";
        Cursor cursor = database.rawQuery(select, new String[]{username, password});
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_USERNAME));
                String userPass = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
                String email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                ModelUser user = new ModelUser(id, name, userPass, email);
                return user;
            }// end whileS
        } // end if
        database.close();


        ModelUser m = new ModelUser(0, "", "", "");
        return m;
    }

    public ModelImage insertImage(ModelImage image, long user_id) {
        database = getWritableDatabase(); // get access to write the database
        ContentValues values = new ContentValues();
        values.put(CLASSIFIY, image.getClassify());
        values.put(NAME, image.getName());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        image.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bArray = bos.toByteArray();
        values.put("image", bArray);
        values.put("classify", image.getClassify());
        System.out.println("what: " + user_id);
        values.put("UserID", user_id);
        //values.put(DATE_IMAGE, image.getImage_date()); TODO: fix line
        long id = database.insert("tblimages", null, values);
        image.set_id(id);
        database.close();
        return image;
    }


    public ArrayList<ModelImage> findImages(long id, int classify) {
        database = getReadableDatabase(); // get access to read the database
        ArrayList<ModelImage> m = new ArrayList<ModelImage>();
        // Two options,
        // since query cannot be created in compile time there is no difference
        //Cursor cursor = database.rawQuery(query, values);
        String select = "SELECT * FROM " + "tblimages" + " WHERE " + USERID + " = ? AND " + CLASSIFIY + " = ?";
        Cursor cursor = database.rawQuery(select, new String[]{String.valueOf(id), String.valueOf(classify)});
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex("name"));
                byte[] bitmapdata = cursor.getBlob(cursor.getColumnIndex("image"));
                Bitmap bmp = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
                int classify_data = cursor.getInt(cursor.getColumnIndex(CLASSIFIY));
                long image_id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                m.add(new ModelImage(image_id, name, bmp, classify_data));
            }// end whileS
        } // end if
        database.close();

        return m;
    }
    public Boolean removeImage(long id)
    {
        database = getWritableDatabase(); // get access to read the database
        Boolean ret = database.delete("tblimages", "_id" + "=" + id, null) > 0;
        database.close();
        return ret;
    }






}